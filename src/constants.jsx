const url = "https://api.github.com/search/repositories?q=stars:%3E=10000";
const sort = "&per_page=40&sort=stars&order=desc";
export { url, sort };
