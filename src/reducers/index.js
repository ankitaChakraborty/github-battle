import update from "react-addons-update";
const reducer = (
  state = {
    loadingBeat: true,
    repo_data: [],
    active: false,
    query: ["", ""],
    search_results: [{ items: [] }, { items: [] }],
    counts: [0, 0],
    battleStarted: false,
    warriors: [{}, {}],
    loadingClip: false
  },
  action
) => {
  switch (action.type) {
    case "UPDATE_STATE": {
      return { ...state, [action.key]: action.value };
    }
    case "HANDLE_SEARCH": {
      return update(state, {
        query: {
          [action.payload.index]: {
            $set: action.payload.value
          }
        }
      });
    }
    case "HANDLE_SEARCH_SUBMIT": {
      return update(state, {
        search_results: {
          [action.payload.index]: {
            $set: action.payload.myJson
          }
        },
        counts: {
          [action.payload.index]: { $set: 0 }
        }
      });
    }
    case "HANDLE_NEXT_RESULT": {
      return update(state, {
        counts: {
          [action.payload]: {
            $set: state.counts[action.payload] + 1
          }
        }
      });
    }
    case "HANDLE_PREV_RESULT": {
      return update(state, {
        counts: {
          [action.payload]: {
            $set: state.counts[action.payload] - 1
          }
        }
      });
    }
    case "HANDLE_BATTLE": {
      return update(state, {
        warriors: {
          [action.payload.index]: { $set: action.payload.myJson }
        },
        loadingClip: { $set: false }
      });
    }
    default:
      return state;
  }
};
export default reducer;
