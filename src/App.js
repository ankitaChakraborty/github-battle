import React, { Component } from "react";
import "./App.css";
import Navbar from "./components/popularsearch/Navbar";
import { Route, Switch } from "react-router-dom";
import PopularSearch from "./components/popularsearch/PopularSearch";
import Battle from "./components/battle/Battle";
class App extends Component {
  state = {
    active: "popular"
  };
  componentDidMount() {
    const active = localStorage.getItem("active");
    this.setActive(active);
  }
  setActive = active => {
    localStorage.setItem("active", active);
    this.setState({ active });
  };
  render() {
    return (
      <div>
        <div className="page-heading">
          <header>
            <h1>Popular Repositories and GitHub Battle</h1>
          </header>
        </div>

        <div>
          <Navbar
            changeActiveStyle={this.changeActiveStyle}
            updateActive={this.setActive}
            active={this.state.active}
          />
        </div>
        <div>
          <Switch>
            <Route
              exact
              path="/"
              render={() => <PopularSearch setActive={this.setActive} />}
            />
            <Route
              path="/battle"
              render={() => <Battle setActive={this.setActive} />}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
