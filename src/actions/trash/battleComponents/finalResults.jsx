import React from "react";
import ResultCards from "./finalResultCards";
const FinalResults = props => (
  <div className="users-entered">
    <ResultCards heading="Winner!" warrior={props.winner} />
    <ResultCards heading="Loser!" warrior={props.loser} />
  </div>
);

export default FinalResults;
