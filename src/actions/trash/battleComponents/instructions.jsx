import React from "react";
const Instructions = props => (
  <React.Fragment>
    {" "}
    {props.index < 2 ? (
      <div className="instructions_child">
        <h2 className="">Instructions</h2>
        <div className="instruction-list">
          <h6>Enter the first GitHub username</h6>
          <h6>Enter the next one</h6>
          <h6>Click "Start Battle" to see the results</h6>
        </div>
      </div>
    ) : null}
  </React.Fragment>
);

export default Instructions;
