import React from "react";
const ResultCards = props => (
  <div className="warriors-wrapper">
    <h1>{props.heading}</h1>
    <div className="warriors">
      <div className="users-entered-avatar">
        <a href={props.warrior.html_url} target="_blank">
          <img
            className="warrior-avatar"
            src={props.warrior.avatar_url}
            alt="not found"
          />
        </a>
      </div>
      <div className="users-entered-name">
        <h4>{props.warrior.name}</h4>
      </div>
      <div className="users-entered-followers">
        <i class="fas fa-users" />
        &nbsp;
        <span>{props.warrior.followers} followers</span>
      </div>
      <div className="users-entered-repos">
        <i class="fas fa-users" />
        &nbsp;
        <span>{props.warrior.public_repos} Repositories</span>
      </div>
    </div>
  </div>
);

export default ResultCards;
