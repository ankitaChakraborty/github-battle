import React, { Component } from "react";

class Home extends Component {
  state = {
    query: ""
  };
  handleSubmitUser = () => {
    if (this.props.index < 2) {
      let url = "https://api.github.com/users/" + this.state.query;
      this.props.fetchUserAPI(url);
      this.setState({ query: "" });
    }
  };

  render() {
    return (
      <div>
        {this.props.index < 2 ? (
          <React.Fragment>
            <div className="user-input-battle">
              <input
                autoFocus
                className="form-control"
                value={this.state.query}
                onChange={e => this.setState({ query: e.target.value })}
                type="text"
                placeholder={"enter user " + (this.props.index + 1)}
              />
              <button
                type="button"
                class="btn btn-primary"
                onClick={() => this.handleSubmitUser()}
              >
                Submit >>
              </button>
            </div>
          </React.Fragment>
        ) : (
          <div className="start-battle-btn-div">
            <button
              class="btn btn-primary"
              onClick={this.props.handleStartBattle}
            >
              Start Battle >>
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default Home;
