import React from "react";

const Result = props => (
  <div className="users-entered">
    {props.user.map((user, index) => (
      <div className="warriors-wrapper">
        <div className="warrior-title">
          <h4>{index === 0 ? "First Warrior" : "Second Warrior"}</h4>
        </div>
        <div className="warriors">
          <div className="users-entered-avatar">
            <a href={user.html_url} target="_blank">
              <img
                className="warrior-avatar"
                src={user.avatar_url}
                alt="not found"
              />
            </a>
          </div>
          <div className="users-entered-name">
            <h4>{user.name}</h4>
          </div>
          <div className="users-entered-followers">
            <i class="fas fa-users" />
            &nbsp;
            <span>{user.followers} followers</span>
          </div>
        </div>
      </div>
    ))}
  </div>
);

export default Result;
