import React, { Component } from "react";
import { Home, Result } from "../../battleComponents";
import FinalResults from "../../battleComponents/finalResults";
import BattleUp from "../battleUpdate";
class Battle extends Component {
  state = {
    error: "",
    user: [],
    battleEnded: false
  };
  componentDidMount() {
    this.props.setActive("battle");
  }
  score_1 = 0;
  score_2 = 0;
  fetchUserAPI = url => {
    fetch(url)
      .then(response => response.json())
      .then(myJson => {
        if (!myJson.id) this.setState({ error: true });
        else this.setState({ user: [...this.state.user, myJson] });
      });
  };
  handleStartBattle = () => {
    this.setState({ battleEnded: true });
  };
  handleReset = () => {
    this.setState({ battleEnded: false, user: [] });
  };
  render() {
    return (
      <div>
        <BattleUp />
        {/* {this.state.battleEnded ? (
          <p />
        ) : (
          <React.Fragment>
            <Home
              error={this.state.error}
              user={this.state.user}
              fetchUserAPI={this.fetchUserAPI}
              index={this.state.user.length}
              handleStartBattle={this.handleStartBattle}
            />
            <Result user={this.state.user} />
          </React.Fragment>
        )}

        {this.state.battleEnded ? (
          <div>
            <FinalResults
              winner={
                this.state.user[0].followers + this.state.user[0].public_repos >
                this.state.user[1].followers + this.state.user[1].public_repos
                  ? this.state.user[0]
                  : this.state.user[1]
              }
              loser={
                this.state.user[0].followers + this.state.user[0].public_repos >
                this.state.user[1].followers + this.state.user[1].public_repos
                  ? this.state.user[1]
                  : this.state.user[0]
              }
            />
            <div className="reset-btn">
              <button className="btn btn-primary" onClick={this.handleReset}>
                Reset >>
              </button>
            </div>
          </div>
        ) : (
          <p />
        )} */}
      </div>
    );
  }
}

export default Battle;
