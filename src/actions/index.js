import { url, sort } from "../constants";

export const updateState = (key, value) => {
  return {
    type: "UPDATE_STATE",
    key,
    value
  };
};
export const fetchAPI = (language = "All") => {
  return dispatch => {
    dispatch(updateState("loadingBeat", true));
    dispatch(updateState("repo_data", []));
    dispatch(updateState("active", language));
    fetch(url + "+language:" + language + sort)
      .then(response => response.json())
      .then(myJson => {
        dispatch(updateState("loadingBeat", false));
        dispatch(updateState("repo_data", myJson.items));
      });
  };
};
export const handleSearch = (e, index) => {
  return {
    type: "HANDLE_SEARCH",
    payload: {
      index: index,
      value: e.target.value
    }
  };
};
export const handleSearchSubmit = (index, value) => {
  let url = "https://api.github.com/search/users?q=" + value;

  return dispatch => {
    dispatch(updateState("counts", [0, 0]));
    dispatch(updateState("warriors", [{}, {}]));
    fetch(url)
      .then(response => response.json())
      .then(myJson => {
        dispatch({
          type: "HANDLE_SEARCH_SUBMIT",
          payload: {
            index: index,
            myJson
          }
        });
      });
  };
};
export const handleNextPrevResult = (action, index) => {
  return {
    type: action === "next" ? "HANDLE_NEXT_RESULT" : "HANDLE_PREV_RESULT",
    payload: index
  };
};
export const handleBattle = (search_results, counts) => {
  return dispatch => {
    dispatch(updateState("battleStarted", true));
    dispatch(updateState("loadingClip", true));
    setTimeout(() => dispatch(handleBattleAfter(search_results, counts)), 8000);
  };
};
export const handleBattleAfter = (search_results, counts) => {
  return dispatch => {
    search_results.map((search_results, index) => {
      fetch(
        "https://api.github.com/users/" +
          search_results.items[counts[index]].login
      )
        .then(response => response.json())
        .then(myJson => {
          dispatch({
            type: "HANDLE_BATTLE",
            payload: {
              index,
              myJson
            }
          });
        });
    });
  };
};
export const handleReset = () => {
  return dispatch => {
    dispatch(updateState("battleStarted", false));
    dispatch(updateState("query", ["", ""]));
    dispatch(updateState("search_results", [{ items: [] }, { items: [] }]));
    dispatch(updateState("counts", [0, 0]));
    dispatch(updateState("warriors", [{}, {}]));
  };
};
