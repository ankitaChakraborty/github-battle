import React from "react";
const Results = props => (
  <div className="battle-results-wrapper">
    <div className="result-heading">
      <h1 className="search-result-heading">
        {props.winnerIndex == props.index ? "Winner!" : "Loser!"}
      </h1>
    </div>
    <div>
      <h2>{props.warrior.name}</h2>
    </div>
    <div>
      <img className="search-option-img" src={props.warrior.avatar_url} />
    </div>
    <div>
      <h6>
        <i class="fas fa-users" />
        &nbsp;
        {props.warrior.followers} &nbsp;followers
      </h6>
    </div>
    <div>
      <h6>
        <i class="fas fa-layer-group" />
        &nbsp;
        {props.warrior.public_repos}&nbsp;public repos
      </h6>
    </div>
  </div>
);

export default Results;
