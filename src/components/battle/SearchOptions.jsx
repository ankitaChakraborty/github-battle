import React from "react";
const SearchOptions = props => (
  <React.Fragment>
    {props.search_results[props.index].items == "" ? null : (
      <React.Fragment>
        <div
          className={props.index == 0 ? "search_options1" : "search_options2"}
        >
          <div>
            <h1 className="search-result-heading">
              {props.index == 0 ? "First" : "Second"} Warrior
            </h1>
            <a
              href={
                props.search_results[props.index].items[props.count].html_url
              }
              target="_blank"
            >
              <h4 style={{ letterSpacing: 3 }}>
                {props.search_results[props.index].items[props.count].login}
              </h4>
            </a>
          </div>
          <div className="search-result-image">
            <button
              className="search-prev-next-btns"
              onClick={
                props.count == 0
                  ? null
                  : () => props.handleNextPrevResult("prev", props.index)
              }
              disabled={props.count == 0 ? true : false}
            >
              <i class="fas fa-chevron-circle-left" />
            </button>
            <a
              href={
                props.search_results[props.index].items[props.count].html_url
              }
              target="_blank"
            >
              <img
                className="search-option-img"
                src={
                  props.search_results[props.index].items[props.count]
                    .avatar_url
                }
              />
            </a>
            <button
              disabled={
                props.count ==
                props.search_results[props.index].items.length - 1
                  ? true
                  : false
              }
              className="search-prev-next-btns"
              onClick={() => props.handleNextPrevResult("next", props.index)}
            >
              <i class="fas fa-chevron-circle-right" />
            </button>
          </div>
        </div>
      </React.Fragment>
    )}
  </React.Fragment>
);
export default SearchOptions;
