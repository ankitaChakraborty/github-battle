import React from "react";

const BattleElements = props => (
  <div className="input-and-search">
    <div className="search-input-box">
      <input
        className="form-control"
        type="text"
        placeholder={
          parseInt(props.index) == 0
            ? "Search for first warrior.."
            : "Search for second warrior.."
        }
        onChange={e => props.handleSearch(e, props.index)}
        autoFocus={props.index === 0 ? true : false}
      />
    </div>
    <div className="search-btn">
      <button
        className="search-button"
        onClick={
          props.query[props.index] == ""
            ? null
            : () =>
                props.handleSearchSubmit(props.index, props.query[props.index])
        }
      >
        <i className="fas fa-search" />
      </button>
    </div>
  </div>
);
export default BattleElements;
