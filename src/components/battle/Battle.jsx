import React, { Component } from "react";
import BattleElements from "./BattleElements";
import SearchOptions from "./SearchOptions";
import { ClipLoader } from "react-spinners";
import Results from "./Results";
import { connect } from "react-redux";
import {
  handleSearch,
  handleSearchSubmit,
  handleNextPrevResult,
  handleBattle,
  handleReset
} from "../../actions";
class BattleUp extends Component {
  componentDidMount = () => {
    this.props.setActive("battle");
  };
  componentDidUpdate = () => {
    console.log(this.props.store);
  };

  render() {
    const { store } = this.props;
    return !store.battleStarted ? (
      <div className="wrapper-battle-ip-op">
        <div className="search-boxes">
          {store.query.map((query, index) => (
            <BattleElements
              index={index}
              query={store.query}
              handleSearch={this.props.handleSearch}
              handleSearchSubmit={this.props.handleSearchSubmit}
            />
          ))}
        </div>
        <div className="search-results">
          {store.query.map((query, index) => (
            <SearchOptions
              index={index}
              search_results={store.search_results}
              count={store.counts[index]}
              handleNextPrevResult={this.props.handleNextPrevResult}
            />
          ))}
        </div>

        <div className="confirm-start-btn">
          {store.search_results[0].items.length < 2 ||
          store.search_results[1].items.length < 2 ? null : (
            <div className="confirm-btn">
              <button
                className="btn-primary"
                onClick={() =>
                  this.props.handleBattle(store.search_results, store.counts)
                }
                disabled={
                  store.search_results[0].items[store.counts[0]].id ==
                  store.search_results[1].items[store.counts[1]].id
                    ? true
                    : false
                }
              >
                Confirm and Start Battle >>
              </button>
            </div>
          )}
        </div>
      </div>
    ) : (
      <React.Fragment>
        <div className="results-container">
          {!store.loadingClip ? (
            store.warriors.map((warrior, index) => (
              <Results
                index={index}
                warrior={warrior}
                winnerIndex={
                  store.warriors[0].followers + store.warriors[0].public_repos >
                  store.warriors[1].followers + store.warriors[1].public_repos
                    ? 0
                    : 1
                }
              />
            ))
          ) : (
            <div className="clip-loader-container">
              <ClipLoader
                className="clip-loader"
                sizeUnit={"px"}
                size={60}
                color={"#123abc"}
                loading={store.loadingClip}
              />
            </div>
          )}
        </div>
        <div className="reset-button-container">
          {!store.loadingClip ? (
            <button className="btn-primary" onClick={this.props.handleReset}>
              Reset >>
            </button>
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    store: state
  };
}

export default connect(
  mapStateToProps,
  {
    handleSearch,
    handleSearchSubmit,
    handleNextPrevResult,
    handleBattle,
    handleReset
  }
)(BattleUp);
