import React, { Component } from "react";
import Buttons from "./Buttons";
import PopularSearchresults from "./PopularSearchResults";
import { BeatLoader } from "react-spinners";
import { connect } from "react-redux";
import { fetchAPI } from "../../actions";

class PopularSearch extends Component {
  componentDidMount = () => {
    this.props.fetchAPI();
    this.props.setActive("popular");
  };

  render() {
    const { store } = this.props;
    return (
      <div>
        <Buttons filterByLanguage={this.props.fetchAPI} active={store.active} />
        <div className="popular-results-wrapper">
          <div
            className={
              store.loadingBeat
                ? "beat-loader-div-visible"
                : "beat-loader-div-hidden"
            }
          >
            <BeatLoader
              className="beat-loader"
              sizeUnit={"px"}
              size={15}
              color={"#123abc"}
              loading={store.loadingBeat}
            />
          </div>

          {store.repo_data.map((repo_data, index) => (
            <PopularSearchresults
              key={index}
              repo_data={repo_data}
              index={index}
            />
          ))}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    store: state
  };
}
export default connect(
  mapStateToProps,
  {
    fetchAPI
  }
)(PopularSearch);
