import React from "react";
const PopularSearchresults = props => (
  <div className="results-cards">
    <div className="avatar">
      <a href={props.repo_data.html_url} target="_blank">
        <img
          className="img-repo"
          src={props.repo_data.owner.avatar_url}
          alt=""
        />
      </a>
    </div>
    <div className="username">
      <a href={props.repo_data.html_url} target="_blank">
        <h4>
          <i class="fas fa-user" />
          &nbsp;
          {props.repo_data.owner.login}
        </h4>
      </a>
    </div>
    <div className="stars-count">
      <h6>
        <i class="fas fa-star" />
        &nbsp;
        {props.repo_data.watchers_count + ` stars`}
      </h6>
    </div>
    <div className="forks-count">
      <h6>
        <i class="fas fa-code-branch" />
        &nbsp;
        {props.repo_data.forks + ` forks`}
      </h6>
    </div>

    <div className="open-issues-count">
      <h6>
        <i class="fas fa-exclamation-circle" />
        &nbsp;
        {props.repo_data.open_issues + ` open issues`}
      </h6>
    </div>
  </div>
);

export default PopularSearchresults;
