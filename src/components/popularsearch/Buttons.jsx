import React, { Component } from "react";
class Buttons extends Component {
  btn_names = ["All", "JavaScript", "Python", "Java", "Ruby", "CSS"];
  render() {
    return (
      <div className="topic-btns-wrapper">
        {this.btn_names.map((btn_names, index) => (
          <button
            className={
              this.props.active === btn_names ? "btn active_btn" : "btn"
            }
            data-toggle="tooltip"
            data-placement="top"
            title={btn_names}
            onClick={() => this.props.filterByLanguage(btn_names)}
            key={btn_names}
          >
            {btn_names}
          </button>
        ))}
      </div>
    );
  }
}

export default Buttons;
