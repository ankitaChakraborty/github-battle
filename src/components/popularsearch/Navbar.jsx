import React from "react";
import { Link } from "react-router-dom";
const Navbar = props => (
  <div className="popular-or-battle-wrapper">
    <div className="pop-bat-nav">
      <Link
        to="/"
        id="nav-link"
        className={props.active === "popular" ? "active" : "link"}
      >
        Popular Repositories
      </Link>
    </div>
    <div className="pop-bat-nav">
      <Link
        to="/battle"
        className={props.active === "battle" ? "active" : "link"}
      >
        GitHub Battle
      </Link>
    </div>
  </div>
);
export default Navbar;
